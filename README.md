# 逆ポーランド電卓を作成する #

### 含まれるもの ###

* stack 関係の関数のためのテスト
* stack 関係の関数本体
* rpncalc (逆ポーランド電卓本体)
* 上記をコンパイルするための makefile

### 手順 ###

* make test でテストプログラムを作成し，実行する
* make で逆ポーランド電卓を作成し，実行する

### リポジトリサービスの利用例の説明文書 ###

* [moodle のブック](http://ee.metro-cit.ac.jp/moodle/mod/book/view.php?id=6222)