// testStack.c by *****

#include <stdio.h>
#include "stack.h"
#include "testCommon.h"

void testInitStack() {
    stack myStack;
    testStart("initStack");
    myStack.top = 0; 
    initStack(&myStack,N);
    assert_equals_int(myStack.top, N);
    testEnd();
}
    void testTopValue(void) {
        stack myStack;
        testStart("topValue");
        initStack(&myStack);
        assert_equals_int(isnan(topValue(&myStack)), 1);
        myStack.top = 0;
        myStack.data[0] = 5.0;
        assert_equals_double(topValue(&myStack), 5.0);
        testEnd();
    
}
int main() {
    testInitStack();
    testTopValue();
	return 0;
}
